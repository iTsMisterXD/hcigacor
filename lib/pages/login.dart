// ignore_for_file: use_build_context_synchronously, avoid_print

import 'package:flutter/material.dart';
import 'package:coding/pages/dashboard.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:coding/util/function.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:encrypt/encrypt.dart' as encrypt;

class Login extends StatefulWidget {
  const Login({Key? key}) : super(key: key);

  @override
  // ignore: library_private_types_in_public_api
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String username = '';
  String password = '';
  late RSAPublicKey serverPublicKey;
  final Book book = Book(); 
  String encryptedPassword2 = '';
  late String fernetKey;


  @override
  void initState() {
    super.initState();
    loadPublicKey();
    initializeFernetKey();
  }

  Future<void> initializeFernetKey() async {
    fernetKey = await book.getFernetKey(); 
  }

  Future<void> login() async {
    try {
      final response = await http.post(Uri.parse("http://192.168.1.2:5000/login"), body: {
        "Username": username,
        "Password": encryptedPassword2,
      });
      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        print(data);
        // data['value'] = await book.decryptValue(data['value'], fernetKey);
        if (data['value'] == '1') {
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(
              builder: (context) => Dashboard(),
            )
          );
        } else {
          ScaffoldMessenger.of(context).showSnackBar(
            SnackBar(
              content: Text(data['message']),
              backgroundColor: Colors.red,
            ),
          );
        }        
      } else {
        print("Failed to send data. Status code: ${response.statusCode}");
      }
    } catch (e) {
      print('error: $e');
    }
  }

  Future<void> loadPublicKey() async {
    try {
      await book.loadPublicKey(); 
      setState(() {
        serverPublicKey = book.serverPublicKey;
      });
    } catch (e) {
      print('Error loading public key: $e');
    }
  }

  Future<void> encryptAndSendData() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        final encryptedPassword = await book.encryptData(password);
        setState(() {
          encryptedPassword2 = encryptedPassword;
        });
        await login();
      } catch (e) {
        print('Error encrypting and sending data: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Login Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: const InputDecoration(
                  labelText: 'Username',
                  prefixIcon: Icon(Icons.email),
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter your Username';
                  }
                  return null;
                },
                onSaved: (value) {
                  setState(() {
                    username = value!;
                  });
                },
              ),
              const SizedBox(height: 20),
              TextFormField(
                decoration: const InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                ),
                obscureText: true,
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Please enter your password';
                  }
                  return null;
                },
                onSaved: (value) {
                  setState(() {
                    password = value!;
                  });
                },
              ),
              const SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                ),
                onPressed: encryptAndSendData,
                child: const Text(
                  'Login',
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void main() => runApp(const MaterialApp(home: Login()));

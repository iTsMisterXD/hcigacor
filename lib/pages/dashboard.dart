// ignore_for_file: avoid_print
import 'package:flutter/material.dart';
import 'package:coding/pages/login.dart';
import 'package:coding/util/validator.dart';
import 'package:coding/util/Navigation.dart';

// ignore: must_be_immutable
class Dashboard extends StatefulWidget {
  Dashboard({super.key});

  @override
  State<Dashboard> createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String uname = '';
  String email = '';
  String password = '';
  String phone = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Dashboard'),
        leading: IconButton(
          icon: const Icon(Icons.logout),
          onPressed: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => Login()));
          },
        ),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Text(
                'Welcome to the Dashboard!',
                style: TextStyle(fontSize: 24),
              ),
              const SizedBox(height: 20),
              Validator.buildFormField(
                'Username',
                Icons.person,
                validation: Validator.validateUsername,
              ),
              const SizedBox(height: 10),
              Validator.buildFormField('Email', Icons.email,
                  validation: Validator.validateEmail,),
              const SizedBox(height: 10),
              Validator.buildFormField('Password', Icons.lock,
                  obscureText: true, validation: Validator.validatePassword),
              const SizedBox(height: 10),
              Validator.buildFormField('Phone Number', Icons.phone,
                  keyboardType: TextInputType.phone,
                  validation: Validator.validatePhoneNumber),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: _submitform,
                child: const Text('Submit'),
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _submitform() {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();
      Navigator.of(context).pushNamed(approutes.destination);
    }
  }
}

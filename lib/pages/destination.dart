import 'package:flutter/material.dart';
import 'package:coding/util/Navigation.dart';

class Destination extends StatelessWidget {
  const Destination({Key? key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Destination'),
        leading: IconButton(
          icon: const Icon(Icons.arrow_back),
          onPressed: () {
            Navigator.pushNamed(context, approutes.dashboard);
          },
        ),
      ),
      body: const Center(
        child: Text('Welcome to the Destination Screen!'),
      ),
    );
  }
}
import 'package:flutter/material.dart';
import 'package:coding/pages/login.dart';
import 'package:coding/util/function.dart'; // Replace with the actual path to your Book file
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:flutter_udid/flutter_udid.dart';
import 'package:pointycastle/asymmetric/api.dart';
import 'package:coding/util/validator.dart';
import 'package:coding/util/Navigation.dart';

class Regis extends StatefulWidget {
  const Regis({Key? key}) : super(key: key);

  @override
  _RegisState createState() => _RegisState();
}

class _RegisState extends State<Regis> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  String ID = '';
  String username = '';
  String password = '';
  late RSAPublicKey serverPublicKey;
  final Book book = Book(); 

  @override
  void initState() {
    super.initState();
    divid();
    loadPublicKey();
  }

  Future<void> loadPublicKey() async {
    try {
      await book.loadPublicKey(); 
      setState(() {
        serverPublicKey = book.serverPublicKey;
      });
    } catch (e) {
      print('Error loading public key: $e');
    }
  }

  Future<void> sendData(String encryptedPassword) async {
    try {
      final response = await http.post(
        Uri.parse("http://192.168.1.2:5000/read"),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode({
          "ID": ID,
          "Username": username,
          "Password": encryptedPassword,
        }),
      );

      if (response.statusCode == 200) {
        final data = json.decode(response.body);
        print(data);
      } else {
        print("Failed to send data. Status code: ${response.statusCode}");
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> divid() async {
    await book.getUID();
    setState(() {
      ID = book.id!;
    });
  }

  Future<void> encryptAndSendData() async {
    if (_formKey.currentState!.validate()) {
      _formKey.currentState!.save();

      try {
        // Encrypt the password
        final encryptedPassword = await book.encryptData(password);

        // Send encrypted data to the server
        await sendData(encryptedPassword);

        Navigator.of(context).pushReplacementNamed(
          approutes.login,
        );
      } catch (e) {
        print('Error encrypting and sending data: $e');
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Registration Page'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(20.0),
        child: Form(
          key: _formKey,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Username',
                  prefixIcon: Icon(Icons.person),
                ),
                validator: Validator.validateUsername,
                onSaved: (value) {
                  setState(() {
                    username = value!;
                  });
                },
              ),
              SizedBox(height: 20),
              TextFormField(
                decoration: InputDecoration(
                  labelText: 'Password',
                  prefixIcon: Icon(Icons.lock),
                ),
                obscureText: true,
                validator: Validator.validatePassword,
                onSaved: (value) {
                  setState(() {
                    password = value!;
                  });
                },
              ),
              SizedBox(height: 20),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                  primary: Colors.blue,
                  padding: EdgeInsets.symmetric(horizontal: 50, vertical: 15),
                ),
                onPressed: encryptAndSendData,
                child: Text(
                  'Register',
                  style: TextStyle(fontSize: 18),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

void main() => runApp(MaterialApp(home: Regis()));

from flask import Flask, jsonify, request
import mysql.connector
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import padding, rsa
import base64
from cryptography.fernet import Fernet

app = Flask(__name__)

private_key_path = 'D:\enkripdekrip\enkripdekrip\private_key.pem'

servername = "127.0.0.1"
username = "root"
password = ""
dbname = "flutterdjanda"

fernetKey = Fernet.generate_key()

def connect_to_database():
    return mysql.connector.connect(
        host=servername,
        user=username,
        password=password,
        database=dbname
    )

def encryptValue(value):
    f = Fernet(fernetKey)
    encrypted = f.encrypt(value.encode())
    return encrypted

# def getvalueformsh256(decrypted_password):

#     password_bytes = decrypted_password.encode('utf-8')

#     sha256 = hashlib.sha256()

#     sha256.update(password_bytes)

#     hashed_bytes = sha256.digest()

#     hashed_base64 = base64.b64encode(hashed_bytes).decode('utf-8')

#     return hashed_base64

def decrypt_password(encrypted_password, private_key_path):
    try:
        print("Cipher Text Length:", len(encrypted_password))
        with open(private_key_path, "rb") as key_file:
            private_key = serialization.load_pem_private_key(
                key_file.read(),
                password=None,
                backend=default_backend()
            )
        
        encrypted_bytes = base64.b64decode(encrypted_password)

        # Decrypt the password
        decrypted_bytes = private_key.decrypt(
            encrypted_bytes,
            padding.PKCS1v15(),
        )

        decrypted_password = decrypted_bytes.decode("utf-8")

        return decrypted_password

    except Exception as e:
        raise Exception("Decryption failed: " + str(e))

def registeredUsername(username, cursor):
    cursor.execute(f"SELECT * FROM db_data WHERE Username='{username}'")
    result = cursor.fetchone()
    return result['COUNT(*)'] > 0

@app.route('/get_key', methods=['GET'])
def get_key():
    return jsonify({"key": fernetKey.decode("utf-8")})

@app.route('/read', methods=['POST', 'GET'])
def handle_data():
    conn = None
    try:
        conn = connect_to_database()
        cursor = conn.cursor(dictionary=True)

        if request.method == 'POST':
            data = request.get_json()
            ID = data.get('ID')
            Username = data.get('Username')
            encrypted_password = data.get('Password')

            try:
                decrypted_password = decrypt_password(encrypted_password, private_key_path)
                # hashed_password = getvalueformsh256(decrypted_password)
            except Exception as decrypt_error:
                return jsonify({"error": str(decrypt_error)})

            if ID is not None and Username is not None and decrypted_password is not None:
                if registeredUsername(Username, cursor):
                    return jsonify({"error": "Username already registered"})
                
                cursor.execute("INSERT INTO db_data (ID, Username, Password) VALUES (%s, %s, %s)", (ID, Username, decrypted_password))
                conn.commit()
                return jsonify({"success": "Data sent successfully"})
            else:
                return jsonify({"error": "Invalid data provided"})

        elif request.method == 'GET':
            cursor.execute("SELECT * FROM db_data")
            data = cursor.fetchall()
            return jsonify(data)

    except Exception as e:
        print("Server-side error:", str(e))
        return jsonify({"error": str(e)})

    finally:
        if conn and conn.is_connected():
            cursor.close()
            conn.close()

@app.route('/login', methods=['POST'])
def login():
    try:
        conn = connect_to_database()
        cursor = conn.cursor(dictionary=True)

        if request.method == 'POST':
            form_username = request.form.get('Username')
            form_password = request.form.get('Password')
            try:
                decrypted_password = decrypt_password(form_password, private_key_path)
                # hashed_password = getvalueformsh256(decrypted_password)
            except Exception as decrypt_error:
                return jsonify({"error": str(decrypt_error)})
            
            if form_username is not None and decrypted_password is not None:
                query = f"SELECT * FROM db_data WHERE Username='{form_username}' AND Password='{decrypted_password}'"
                cursor.execute(query)
                result = cursor.fetchone()

                if result:
                    response = {'value': '1', 'message': 'Login Berhasil'}
                else:
                    response = {'value': '0', 'message': 'NIM atau Password Salah'}

                return jsonify(response)

            else:
                return jsonify({"error": "Invalid data provided"})

    except Exception as e:
        return jsonify({"errorr": str(e)})

    finally:
        if conn.is_connected():
            cursor.close()
            conn.close()

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)

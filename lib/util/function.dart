import 'dart:io';
import 'package:encrypt/encrypt.dart';
import 'package:flutter_udid/flutter_udid.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:encrypt/encrypt.dart' as encrypt;
import 'package:pointycastle/asymmetric/api.dart';
import 'package:pointycastle/asymmetric/rsa.dart';
import 'package:pointycastle/export.dart';
import 'package:pointycastle/pointycastle.dart';
import 'dart:typed_data';

class Book {
  String? _id;
  List listData = [];
  late RSAPublicKey serverPublicKey;

  String? get id => _id;

  Future<void> getUID() async {
    String udid = await FlutterUdid.udid;
    _id = udid;
  }

  Future<void> getdata() async {
    try {
      final response = await http.get(
          Uri.parse("http://192.168.1.2:5000/read"));
      if (response.statusCode == 200) {
        final List<Map<String, dynamic>> data = (json.decode(response.body) as List)
            .cast<Map<String, dynamic>>();
        List idList = data.map((item) => item['ID']).toList();
        listData = idList;
      } else {
        print("Error");
      }
    } catch (e) {
      print(e);
    }
  }

  final String publicKeyString = '''-----BEGIN PUBLIC KEY-----
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAp1HFiQtDvsi835W5SzkN
5RVa1HtzL4U17dwagrrcFnVjQrn+HT+TJWGdT0ldACvriuxyeoLxHC42aXdB7ZyS
O3Sir2g77PsKgBDPGqo7Cqc+8BwyIGQ+q458wW7YA4EkvW3h+OXUMiyRSbLPexbB
stxg9x7eogYjRKblAskqrZ8FY22VJn08iDBGU7o1UpwrW23h93P8j7Pg8Gu/i2HV
qFuYP5a1D25pQ3bp699sydw/7zFi3NnIJbyvFKPM8dPG+lqpXTtUxAbO9pgV5mHI
Ip2WVARqB9TmL1NTXMRIhC2dDfhWCsxyd4nasyQOBvMZv9pd8Dep3AjvluRsilC/
NQIDAQAB
-----END PUBLIC KEY-----''';

  Future<void> loadPublicKey() async {
    try {
      serverPublicKey =
          RSAKeyParser().parse(publicKeyString) as RSAPublicKey;
      print('Public key loaded successfully');
    } catch (e) {
      print('Error loading public key: $e');
    }
  }

  // String hashPassword(String password) {
  //   final sha256 = SHA256Digest();
  //   final List<int> passwordBytes = utf8.encode(password);

  //   final Uint8List passwordUint8List = Uint8List.fromList(passwordBytes);

  //   final Uint8List hashedBytes = sha256.process(passwordUint8List);

  //   return base64Encode(hashedBytes);
  // }

  // Encrypt password
  Future<String> encryptData(String password) async {
    if (serverPublicKey == null) {
      throw Exception('Public key not loaded');
    }

    final hashedPassword = password;

    final encrypter = encrypt.Encrypter(encrypt.RSA(publicKey: serverPublicKey));
    final encrypted = encrypter.encrypt(hashedPassword);
    
    return encrypted.base64;
  }

  String decryptValue(String encryptedValue, String key) {
    final fernetKey = Key.fromUtf8(key);
    final b64key = base64Url.encode(fernetKey.bytes).substring(0, 32);
    final fernet = Fernet(b64key as Key);
    final encrypter = Encrypter(fernet);
    final decrypted = encrypter.decrypt(Encrypted.fromBase64(encryptedValue));
    return decrypted;
  }

  Future<String> getFernetKey() async {
    final response = await http.get(
          Uri.parse("http://192.168.1.2:5000/get_key"));
    if (response.statusCode == 200) {
      final keyJson = json.decode(response.body);
      return keyJson['key'];
    } else {
      throw Exception('Failed to retrieve Fernet key');
    }
  }
}

import 'package:flutter/material.dart';

class Validator {
  String uname = '';
  String email = '';
  String password = '';
  String phone = '';

  static String? validateEmail(String? value) {
    if (value == null || value.isEmpty) {
      return 'Email is required';
    }
    if (!RegExp(r'^[\w-\.]+@([\w-]+\.)+[\w-]{2,4}$').hasMatch(value)) {
      return 'Please enter a valid email address';
    }
    return null;
  }

  static String? validateUsername(String? value) {
    if (value == null || value.isEmpty) {
      return 'Username is required';
    }
    if (value.length < 6) {
      return 'Username must be at least 6 characters long';
    }
    return null;
  }
  
  static String? validatePassword(String? value) {
  if (value == null || value.isEmpty) {
    return 'Password is required';
  }
  if (value.length < 8) {
    return 'Password must be at least 8 characters long';
  }

  final passwordRegex = RegExp(r'^(?=.*[0-9])(?=.*[a-zA-Z])');
  if (!passwordRegex.hasMatch(value)) {
    return 'Password must contain a combination of letters and numbers';
  }
  return null;
  }

  static String? validatePhoneNumber(String? value) {
  if (value == null || value.isEmpty) {
    return 'Phone number is required';
  }

  final phoneNumberRegex = RegExp(r'^[0-9]{10}$');
  if (!phoneNumberRegex.hasMatch(value)) {
    return 'Phone number must be 10 digits long and contain only numbers';
  }
  return null;
  }

  static Widget buildFormField(
    String labelText,
    IconData iconData, {
    bool obscureText = false,
    TextInputType? keyboardType,
    String? Function(String?)? validation,
  }) {

    return TextFormField(
      autovalidateMode: AutovalidateMode.onUserInteraction,
      decoration: InputDecoration(
        labelText: labelText,
        icon: Icon(iconData),
        border: OutlineInputBorder(),
      ),
      obscureText: obscureText,
      keyboardType: keyboardType,
      validator: (value) => validation!(value),
      onSaved: (value) {},
    );
  }
}

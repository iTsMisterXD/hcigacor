import 'package:flutter/material.dart';
import 'package:coding/pages/login.dart';
import 'package:coding/pages/regis.dart';
import 'package:coding/pages/dashboard.dart';
import 'package:coding/pages/destination.dart';

class approutes {
  static const String login = '/login';
  static const String regis = '/regis';
  static const String dashboard = '/dashboard';
  static const String destination = '/destination';

  static Map<String, WidgetBuilder> routes = {
    approutes.login: (context) => Login(),
    approutes.regis: (context) => Regis(),
    approutes.dashboard: (context) => Dashboard(),
    approutes.destination: (context) => Destination(),
  };
}